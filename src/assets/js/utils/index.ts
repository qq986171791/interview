import * as dom from './dom';

let domReadyCache = [];

let ins = {
    init(){
        this.addListen();
    },
    getById(id){
        return document.getElementById(id)
    },
    getBySelector(selector){
        return document.querySelector(selector)
    },
    addListen(){
        this.on('DOMContentLoaded',() => {
            this.each(domReadyCache,(item,i) => {
                item();
            })
        });
    },
    domReady(callback){
        if(!this.isFunction(callback)){
            throw 'domReady expect a function'
            return;
        }
        domReadyCache.push(callback);
    },
    each(o,callback){
        if(this.isObject(o)){
            for(let k in o){
                callback(o[k],k);
            }
        }
        if(this.isArray(o) || this.isNodeList(o)){
            for(let i = 0 ;i < o.length ; i++){
                callback(o[i],i);
            }
        }
    },
    isNodeList(o){
        return this.getTypeString(o) == '[object NodeList]'
    },
    isObject(o){
        return this.getTypeString(o) == '[object Object]'
    },
    isArray(o){
        return this.getTypeString(o) == '[object Array]'
    },
    isString(o){
        return this.getTypeString(o) == '[object String]'
    },
    isFunction(o){
        return this.getTypeString(o) == '[object Function]';
    },
    isNull(o){
        return this.getTypeString(o) == '[object Null]';
    },
    getTypeString(o){
        return Object.prototype.toString.call(o);
    },
    on(type,callback){
        callback = callback || function(){}
        if ( document.addEventListener ) {
            document.addEventListener( type, callback );
        } else if ( document.readyState === "complete" ) {
            document['detachEvent']( type, callback );
        }
    },
    off(){

    }
}

ins.init();

export default ins

export {dom}