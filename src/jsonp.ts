import './assets/css/index.scss';
import {default as utils,dom} from './assets/js/utils'
const _ = require('lodash');
const fs = require('fs');

class Jsonp{
    constructor(opts){
        _.merge(this,opts);
        this.init();
    }
    init(){
        this.initCallback();
        this.initScript();
    }
    initScript(){
        let script =  this.createEl('script',{src:this.url})
        document.head.append(script);
    }
    initCallback(){
        window.tycallback = (ret) => {
            this.callback(ret)
        }
    }
    createEl(tag,attrs,children:Array=[]){
        let node = document.createElement(tag);
        utils.each(attrs,(item,key) => {
            node.setAttribute(key,item);
        })
        if(children.length){
            utils.each(children,(item,key) => {
                let child = null;
                if(utils.isString(item)){
                    child = document.createTextNode(item);
                }else{
                    child = this.createEl(item.tag,item.attrs,item.children)
                }
                node.append(child);
            })
        }
        return node;
    }
}

window.JSONP = (url,cfg) => {
    cfg.url = url;
    new Jsonp(cfg);
}

JSONP( 'https://api.flickr.com/services/feeds/photos_public.gne?tags=cat&tagmode=any&format=json&jsoncallback=tycallback',{
    data:{},
    callback:function (ret) {
        console.log(ret);
    }
});

