import './assets/css/index.scss';
import {default as utils,dom} from './assets/js/utils'
const _ = require('lodash');
const fs = require('fs');
import slide1 = require( './assets/img/slide_1.jpg');
import slide2 = require( './assets/img/slide_2.jpg');
import slide3 = require( './assets/img/slide_3.jpg');
import slide4 = require( './assets/img/slide_4.jpg');


class Q{
    arr:Array = [];
    slide:any;
    constructor(arr,slide){
        this.arr = arr;
        this.slide = slide;
    }
    lIn(v){
        let first = this.arr[0];
        this.arr.unshift(v);
        this.slide.element.insertBefore(v.val.el,first.val.el)
    }
    lOut(){
        if(!this.arr.length){return}
        let item = this.arr.shift();
        this.slide.element.querySelector('li').remove();
    }
    rIn(v){
        this.arr.push(v);
        this.slide.element.append(v.val.el);
    }
    rOut(){
        if(!this.arr.length){return}
        let item = this.arr.pop();
        let lis = this.slide.element.querySelectorAll('li');
        lis[lis.length - 1].remove();
    }
}
class Node{
    pre:any
    next:any
    val:any
    constructor(val){
        this.val = val;
    }
}
class CLink{
    cur:null;
    headVal:null;
    head:any = null;
    constructor(arr){
        this.headVal = arr[0];
        this.head = new Node(this.headVal);
        this.head.next = this.head;

        this.cur = this.head.next;

        this.initVal(arr);
    }
    initVal(arr){
        let cur = this.head;
        utils.each(arr,(item,i) => {
            if(i > 0){
                this.insert(item,cur.val);
                cur = cur.next;
            }
        });
    }
    find(val){
        let cur = this.head;
        while(!utils.isNull(cur.next) && !(cur.next.val == val)){
            cur = cur.next;
        }
        return cur.next;
    }
    findLast(){
        let cur = this.head;
        while(!utils.isNull(cur.next) && !(cur.next.val == this.headVal)){
            cur = cur.next;
        }
        return cur;
    }
    updateLast(){
        let last = this.findLast();
        last.next = this.head;
        this.head.pre = last;
    }
    insert(newVal,refVal){
        let newNode = new Node(newVal),
            cur = this.find(refVal);
        newNode.next = cur.next;
        newNode.pre = cur;
        cur.next = newNode;
        this.updateLast();
    }
    remove(val){
        let cur = this.find(val);
        if(utils.isNull(cur) || cur.val == this.headVal){
            return;
        }
        if(utils.isNull(cur.next) || cur.next.val == this.headVal){
            return;
        }
        if(utils.isNull(cur.pre) || cur.pre.val == this.headVal){
            return;
        }
        cur.pre.next = cur.next;
        cur.next = null;
        cur.pre = null;
        this.updateLast();
    }
}
class Slide{
    element:Element;
    dom:any = {
        domLi:'dom-li',
        domBtn:'dom-btn',
        domDot:'dom-dot'
    };
    img:Array = []
    elWidth:number
    elHeight:number
    clink:any = null;
    q:any = null;
    finish:boolean = true;
    on:any; //当前选中
    dots:any;
    constructor(opts){
        _.merge(this,opts);
        this.init();
    }
    init(){
        this.setElWidth(parseInt(dom.getStyle(this.element,'width')))
        this.initBtn();
        this.initLink(this.img)
        this.initDot();
        this.initQ([this.clink.cur.pre,this.clink.cur,this.clink.cur.next]);
        this.render();
        this.initDom();
        this.initDotContainer();
        this.initContainerStyle()
        this.initLiStyle();
        this.addListen();
    }
    initDot(){
        let dots = [];
        for(let i = 0; i < this.img.length; i++){
            dots.push({
                tag:'i',
                attrs:{
                    style:'display:block;width:10px;height:10px;background:red;border-radius:50%;margin:0 10px;',
                    'dom-dot':''
                },
                children:[]
            });
        }
        this.dots = dots;
        // let height = dom.getStyle(this.dom.domLi[0],'height');

    }
    initDotContainer(){
        this.dom.domLi[0].querySelector('img').onload =  () => {
            this.setElHeight(parseInt(dom.getStyle(this.dom.domLi[0],'height')));
            dom.setStyle(this.element,'height',`${this.getElHeight()}px`);
            let dotContainer = this.createEl('p',{style:`z-index:2;width:100%;top: ${this.getElHeight() - 30}px;display:flex;justify-content: center;position: absolute;`},this.dots);
            this.element.append(dotContainer);
        }
    }
    setElHeight(v){
        this.elHeight = v;
    }
    getElHeight(){
        return this.elHeight;
    }
    initContainerStyle(){
        dom.setStyle(this.element,'position','relative');
    }
    initLink(arr){
        utils.each(arr,(item,i) => {
            let left = i * this.elWidth;
            let li = this.createEl('li',{class:'i','dom-li':'',style:'width:100%;'},[
                {
                    tag:'img',
                    attrs:{src:item,style:'width:100%;'},
                    children:[]
                }
            ])
            arr[i] = {
                img:item,
                el:li,
                index:i
            };
        });
        this.clink = new CLink(arr);
    }
    initQ(arr){
        this.q = new Q(arr,this);
    }
    initBtn(){
        let btnLeft = this.createEl('button',{'dom-btn':'left'},[
            "向左"
        ])
        let btnRight = this.createEl('button',{'dom-btn':'right'},[
            "向右"
        ])

        dom.setStyle(btnLeft,{position:'absolute',left:0,top:`90px`,zIndex:'2'});
        dom.setStyle(btnRight,{position:'absolute',right:0,top:`90px`,zIndex:'2'});

        this.element.append(btnLeft);
        this.element.append(btnRight);
        //pre
        dom.on(btnLeft,'click',() => {
            if(!this.isFinish()){
                return;
            }
            this.toX('pre')
        });
        //next
        dom.on(btnRight,'click',() => {
            if(!this.isFinish()){
                return;
            }
            this.toX('next')
        });

    }
    createEl(tag,attrs,children:Array=[]){
        let node = document.createElement(tag);
        utils.each(attrs,(item,key) => {
            node.setAttribute(key,item);
        })
        if(children.length){
            utils.each(children,(item,key) => {
                let child = null;
                if(utils.isString(item)){
                    child = document.createTextNode(item);
                }else{
                    child = this.createEl(item.tag,item.attrs,item.children)
                }
                node.append(child);
            })
        }
        return node;
    }
    //渲染
    render(){
        utils.each(this.q.arr, (item,i) => {
            this.element.append(item.val.el);
        });
    }
    initLiStyle(){
        utils.each(this.dom.domLi, (item,i) => {
            let left = i * this.elWidth - this.elWidth;
            dom.setStyle(item,'left',`${left}px`);
        });
    }
    getLiCount(){
        return this.dom.domLi.length;
    }
    toX(d){
        utils.each(this.q.arr, (item,i) => {
            let keyframes = [],
                startX = parseInt(dom.getStyle(item.val.el,'left')),
                endX = 0;
            if(d == 'pre'){
                endX = startX - this.getElWidth();
                keyframes = [
                    { left: `${startX}px` },
                    { left: `${endX}px` },
                ];
            }
            if(d == 'next'){
                endX = startX + this.getElWidth();
                keyframes = [
                    { left: `${startX}px` },
                    { left: `${endX}px` },
                ];
            }
            this.finish = false;
            let ani = item.val.el.animate(keyframes, {
                easing:'ease-in-out',
                duration: 1000,
                fill: "forwards"
            })
            ani.onfinish = () => {
                this.finish = true;
                if(!this.isFirstAnimate(i)){
                    return;
                }
                if(d == 'pre'){
                    this.clink.cur = this.clink.cur.next;

                    let next = this.clink.cur.next;

                    this.q.rIn(this.cloneNode(next));

                    this.q.lOut();
                }
                if(d == 'next'){
                    this.clink.cur = this.clink.cur.pre;

                    let pre = this.clink.cur.pre;

                    this.q.lIn(this.cloneNode(pre,'right'));

                    this.q.rOut();
                }
                this.setOn(this.clink.cur);
                this.renderOnDot();
            };
        });
    }
    setOn(v){
        this.on = v;
    }
    getOn(){
        return this.on;
    }
    renderOnDot(){
        let index = this.on.val.index;
        this.resetDot();
        utils.each(this.dom.domDot,(item,i) => {
            if(i == index){
                dom.setStyle(item,{background:'black'});
            }
        });
    }
    resetDot(){
        let index = this.on.val.index;
        utils.each(this.dom.domDot,(item,i) => {
            if(i != index){
                dom.setStyle(item,{background:'red'});
            }
        });
    }
    cloneNode(node:any,d?:string='left'){
        let newNode = null;
        if(d == 'left'){
            let pre = node.pre,
                preLeft = parseInt(dom.getStyle(pre.val.el,'left'));
            newNode = node;
            node.val.el = node.val.el.cloneNode(true);
            dom.setStyle(newNode.val.el,'left',`${preLeft + this.elWidth}px`);
        }
        if(d == 'right'){
            let next = node.next,
                nextLeft = parseInt(dom.getStyle(next.val.el,'left'));
            newNode = node;
            node.val.el = node.val.el.cloneNode(true);
            dom.setStyle(newNode.val.el,'left',`${nextLeft - this.elWidth}px`);
        }
        return newNode;
    }
    setElWidth(w){
        this.elWidth = w;
    }
    getElWidth(){
        return this.elWidth;
    }
    initDom(){
        utils.each(this.dom,(item,key) => {
            console.log(item,key);
            let ret = this.element.querySelectorAll(`[${item}]`);
            if(ret.length == 1){
                this.dom[key] = ret[0]
                return;
            }
            this.dom[key] = ret;
        });
    }
    addListen(){

    }
    isFinish(){
        return this.finish;
    }
    isFirstAnimate(i:number){
        return i == 0;
    }
}

utils.domReady(() => {
    /*
    @params element type:Element    轮播容器 必须ul
    @params img     type:String     图片地址
    * */
    let slide = new Slide({
        element:utils.getById('panel-slider'),
        img:[
            slide1,
            slide2,
            slide3,
            slide4,
        ]
    });
})
